#!/usr/bin/python

import requests
from bs4 import BeautifulSoup

url = "http://en.wikiquote.org/wiki/Jeeves_and_Wooster"


r = requests.get(url)
contents = r.content

soup = BeautifulSoup(contents)


dds = soup.find_all('dl')
for dd in dds:
    text = dd.get_text().encode('ascii', 'ignore')
    print text
    print '%'
